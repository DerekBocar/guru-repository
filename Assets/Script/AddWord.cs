﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddWord : MonoBehaviour {

    public GameObject MainMenuObj;
    public GameObject GameCanvasObj;
    public Animator Camera;
    public Transform TermsGrid;
    public GameController Controller;
    public GameObject TermTextPrefab;


    [Header("others")]
    public InputField WordToAdd;
    public InputField MeaningToAdd;
    public List<string> Words;
    public List<string> Meanings;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnClickAdd()
    {

        if (!WordToAdd.text.Equals("") || !MeaningToAdd.text.Equals(""))
        {
            string wta = WordToAdd.text.Replace(" ", "");
            wta = wta.ToUpper();
            Words.Add(wta);
            Meanings.Add(MeaningToAdd.text);
            WordToAdd.text = "";
            MeaningToAdd.text = "";

            GameObject TTExt = Instantiate(TermTextPrefab);

            TTExt.GetComponent<Text>().text = wta;

            TTExt.transform.SetParent(TermsGrid);
            TTExt.transform.localScale = Vector2.one;

        }
    }

    public void OnClickDone()
    {
        if (Words.Count != 0)
        {
            Camera.SetTrigger("Done");
            //MainMenuObj.SetActive(false);
            GameCanvasObj.SetActive(true);
            Controller.GetWordFromList();
            Controller.AddScore(false);
        }
    }



}
