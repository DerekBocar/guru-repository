﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LetterButton : MonoBehaviour
{
    public string Value;


    public void OnClickLetterButton()
    {
        GameObject.Find("GameController").GetComponent<GameController>().WriteAns(Value);
        Destroy(this.gameObject);
    }

    public void SetValue(string s)
    {
        s = s.ToUpper();
        Value = s;
    }

}
