﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public GameObject ButtonLetterPrefab;
    public GameObject DispLetterPrefab;
    public GameObject Panel;
    public GameObject CloseP;
    public Animator Camera;

    public Transform AnsGrid;
    public Transform ButtonGrid;
    public string PlayerAns;
    public string CurrentWord = "";
    public int TotalScore;
    public int QuestionCount = 0;
    public Text ScoreTextUI;
    [Header("feedback")]
    public GameObject WrongUI;
    public GameObject CorrectUI;
    public Image QBar;
    public Text FinalScoreUI;
    [Header("main menu stuff")]
    public Text MeaningText;
    public AddWord AddScript;

    public List<string> ShuffledWord;
    public List<int> PickedWordsList;



	// Use this for initialization
	void Start () {
        AddScore(false);
        QBar.fillAmount = 0;
    }

    public void WriteAns(string letter)
    {
        
        GameObject Disp = Instantiate(DispLetterPrefab);
        Disp.transform.GetChild(0).GetComponent<Text>().text = letter;

        Disp.transform.SetParent(AnsGrid);
        Disp.transform.localScale = Vector2.one;

        PlayerAns += letter.ToUpper();

        if(CurrentWord.Length == PlayerAns.Length)
        {
            Compare();

        }
    }

    void Compare()
    {
        if (CurrentWord.Equals(PlayerAns))
        {

            //UI feedback ex Correct
            //else
            //word is wrong
            //getword fromlist
            StartCoroutine(ScoreCoroutine(true));

        }
        else
        {
            StartCoroutine(ScoreCoroutine(false));
        }
    }


    public void ClearGrid(Transform gridToClear)
    {
        foreach (Transform t in gridToClear)
        {
            GameObject.Destroy(t.gameObject);
        }
    }

    public void GetWordFromList()
    {
        int index = 0;

        while (true)
        {
            index = Random.Range(0, AddScript.Words.Count);
            if (!PickedWordsList.Contains(index))
            {
                PickedWordsList.Add(index);
                break;
            }
        }

        string randWord = AddScript.Words[index];
        MeaningText.text = AddScript.Meanings[index];
        CurrentWord = randWord.ToUpper();

        for (int i = 0; i < randWord.Length; i++)
        {
            ShuffledWord.Add(randWord[i].ToString());
        }
        for (int t = 0; t < ShuffledWord.Count; t++)
        {
            string tmp = ShuffledWord[t];
            int r = Random.Range(t, ShuffledWord.Count);
            ShuffledWord[t] = ShuffledWord[r];
            ShuffledWord[r] = tmp;
        }
        for(int i=0; i<ShuffledWord.Count;i++)
        {
            GameObject newButton = Instantiate(ButtonLetterPrefab);
            newButton.GetComponent<LetterButton>().SetValue(ShuffledWord[i]);

            newButton.transform.GetChild(0).GetComponent<Text>().text = ShuffledWord[i];
            newButton.transform.SetParent(ButtonGrid);
            newButton.transform.localScale = Vector2.one;
        }
     
    }

    public void AddScore(bool isCorrect) 
    {
        if(isCorrect)
            TotalScore++;


        Debug.Log(AddScript.Words.Count);
        ScoreTextUI.text = ("Score: " + TotalScore + "/" + AddScript.Words.Count);
    }

    public void SetButtonGrid()
    {



    }

    IEnumerator ScoreCoroutine(bool isCorrect)
    {
        AddScore(isCorrect);
        Debug.Log("cur: " + CurrentWord + " ans: " + PlayerAns);



        if (!isCorrect)
            WrongUI.SetActive(true);
        else
            CorrectUI.SetActive(true);


        PlayerAns = "";
        CurrentWord = "";
        yield return new WaitForSeconds(1f);
        QuestionCount++;
        QBar.fillAmount = (float) QuestionCount / (float) AddScript.Words.Count;

        WrongUI.SetActive(false);
        CorrectUI.SetActive(false);

        ClearGrid(ButtonGrid);
        ClearGrid(AnsGrid);
        ShuffledWord.Clear();
        MeaningText.text = "";

        if (QuestionCount != AddScript.Words.Count)
        {
            GetWordFromList();


        }
        else
        {
            ScoreTextUI.gameObject.SetActive(false);
            FinalScoreUI.gameObject.SetActive(true);

            FinalScoreUI.text = "FINAL SCORE:\n" + TotalScore + " / " + AddScript.Words.Count;
            Debug.Log("ALL QUESTIONS CLEAR.");

        }

    }
    public void PlayB()
    {
        Panel.SetActive(true);
        CloseP.SetActive(true);

    }
    public void Close()
    {
        Panel.SetActive(false);
        CloseP.SetActive(false);
    }
    public void Language()
    {
        Camera.SetTrigger("Play");

    }

}
